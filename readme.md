# Simple Log Viewer

SLV is used to read log files from applications in real-time.

The main purpose of SLV is to make debugging easier and this is achieved by highlighting text rows that are important to you. SLV can look for specific words and change the text and background colors of that row so you can find it easier.

![Alt text](http://i.imgur.com/Q6S9XuV.png)

The text to search for and their corresponding colors can easily be customized within the configuration file.

![Alt text](http://i.imgur.com/P4U7z8H.png)

## License
Simple Log Viewer is under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).  

You can basically do anything you like with this code as long you include my original copyright and license notice in any copy of the software/source.

## Dependencies
* [colorama 0.3.7](https://pypi.python.org/pypi/colorama)

* [pyinstaller 3.2](http://www.pyinstaller.org/) [Compiling only]

PyInstaller is only needed if you are going to compile the program.

## Usage
To use SLV you need to launch it through the command prompt. If you are planing on using SLV regularly, it's recommended to add it to the system path.
~~~~
slv logfile.log [-load] [-nocolor]

-load       Loads existing text and prints it.
-nocolor    Disables the use of colors.
~~~~

## Configuration
When SLV is started for the first time do two things. It will create a new folder called **config**, inside this folder it will create a configuration file called **config.cfg**. This is the current *(v.1.0)* default configuration.
~~~~
[SLV]
default_colors = [Fore.WHITE, Back.BLACK]
rules = {"DEBUG": [Fore.CYAN, Back.BLACK], "INFO": [Fore.WHITE, Back.BLACK], "WARNING": [Fore.YELLOW, Back.BLACK], "ERROR": [Fore.RED, Back.BLACK], "CRITICAL": [Fore.WHITE, Back.RED]}
~~~~  

This file can be modified to suit your needs, but remember to follow the same text formatting or the configuration might not be read correctly by SLV.  

**Fore** stands for foreground and is the text color and **Back** stands for background and is the background color.

To see all available colors check the [colorama documentation](https://pypi.python.org/pypi/colorama).  

### Changing the rules
~~~~
rules = {"DEBUG": [Fore.CYAN, Back.BLACK]}
~~~~

The rules only consist of two major parts, the specific text to search for and the colors*(foreground, background)*. In this case the text is *DEBUG* **(The text is case sensitive!)** and the colors are cyan and black.

## Compiling
If you would like to compile SLV you will need PyInstaller. The PyInstaller compiling instructions are located inside the **compile** folder.  

It's recommend to use the **-F** parameter in order to create a standalone executable.
~~~~
pyinstaller -F slv.spec
~~~~