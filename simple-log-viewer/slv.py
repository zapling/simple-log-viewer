#
# Simple LogFile Viewer
# Created by Andreas Palm
# https://bitbucket.org/zapling/
#
# The MIT License (MIT)
# Copyright (c) 2016 Andreas Palm
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
from colorama import init
from lib.configuration.slv_config import SlvConfig
from lib.file_handler import FileHandler

program_info = "Simple Log Viewer\n" \
               "slv logfile.log [-load][-nocolor]\n" \
               "\n" \
               "-load       Loads existing text and prints it.\n" \
               "-nocolor    Disables the use of colors."


if __name__ == '__main__':
    init()  # Initialize colorama
    config = SlvConfig().config

    if len(sys.argv) == 1:

        print program_info
        sys.exit(0)

    else:
        file_handler = FileHandler(sys_args=sys.argv, config=config)
