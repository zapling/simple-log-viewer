class FileNotFound(Exception):
    """
    File could not be found.
    """
    pass