from colorama import Fore, Back


class PrintHandler(object):

    def __init__(self, sys_args, config):
        self._color_print = True
        self._config = config

        if "-nocolor" in sys_args:
            self._color_print = False

        self._default_colors = eval(self._config.get('SLV', 'default_colors'))

        self._error_levels = eval(self._config.get('SLV', 'rules'))

    def print_to_screen(self, line):

        if self._color_print:
            printed = False
            for log_level, colors in self._error_levels.iteritems():

                if log_level in line:
                    print ''.join(colors) + line,
                    printed = True

            if printed is False:
                print ''.join(self._default_colors) + line,

        else:
            print line,

