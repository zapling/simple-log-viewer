from lib.exceptions import FileNotFound
from lib.print_handler import PrintHandler
import sys
import time


class FileHandler(object):

    def __init__(self, sys_args, config):
        self._print_handler = PrintHandler(sys_args=sys_args, config=config)
        self._filename = sys_args[1]

        try:
            self._logfile = open(self._filename, "r")

            if "-load" in sys_args:
                self.load()

            lines = self.read()

            for line in lines:
                self._print_handler.print_to_screen(line=line)

        except IOError:
            raise FileNotFound

        except KeyboardInterrupt:
            sys.exit(0)

    def load(self):

        for line in self._logfile.readlines():
            self._print_handler.print_to_screen(line=line)

    def read(self):
        self._logfile.seek(0, 2)
        while True:
            line = self._logfile.readline()
            if not line:
                time.sleep(0.1)
                continue
            yield line
