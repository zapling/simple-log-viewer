from lib.configuration.config import Config


class SlvConfig(Config):

    def __init__(self):
        super(SlvConfig, self).__init__(config_dir="config", config_filename="config")

    def _initialize_defaults(self):

        self._config.add_section('SLV')
        self._config.set('SLV', 'default_colors', '[Fore.WHITE, Back.BLACK]')
        self._config.set('SLV', 'rules', '{"DEBUG": [Fore.CYAN, Back.BLACK], "INFO": [Fore.WHITE, Back.BLACK], "WARNING": [Fore.YELLOW, Back.BLACK], "ERROR": [Fore.RED, Back.BLACK], "CRITICAL": [Fore.WHITE, Back.RED]}')