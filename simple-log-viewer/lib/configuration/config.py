import ConfigParser
import os
import sys


class Config(object):

    def __init__(self, config_dir, config_filename):
        self._config = ConfigParser.RawConfigParser(allow_no_value=True)

        self._config_dir_name = config_dir  # Config directory.
        self._config_filename = config_filename + ".cfg"  # Config filename.
        self._path = os.path.dirname(os.path.realpath(sys.executable))  # Path to script.

        self._initialize_defaults()
        self._load_existing_config()

    def _load_existing_config(self):
        """
        Checks if all necessary folders and the config exists.
        If anything is missing it will create it.
        """
        # Check if the directory is missing.
        if not os.path.exists(self._path + "\\" + self._config_dir_name):
            os.makedirs(self._path + "\\" + self._config_dir_name)
            self._write_config()

        # Check and load existing config.
        elif os.path.exists(self._path + "\\" + self._config_dir_name):
            self._config.read(self._path + "\\" + self._config_dir_name + "\\" + self._config_filename)
        else:
            # Config dir was empty, generating config.
            self._write_config()

    def set_config_value(self, section, option, value):
        """
        Set a configuration value.
        :param section: (str) The configuration section.
        :param option: (str) The configuration option.
        :param value: (str) The configuration
        """
        self._config.set(section=section, option=option, value=value)
        self._write_config()

    def _initialize_defaults(self):
        """
        Initialize the default configuration values.
        These values will be used if they are not defined in an existing config.
        """
        pass

    def _write_config(self):
        """
        Create the config file.
        """
        with open(self._path + "\\" + self._config_dir_name + "\\" + self._config_filename, 'w+') as configfile:
            self._config.write(configfile)

    @property
    def config(self):
        """
        Gets the loaded config.
        :return: (ConfigParser)
        """
        return self._config
